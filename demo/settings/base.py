"""
Django settings for demo project.

Generated by 'django-admin startproject' using Django 2.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

import environ

BASE_DIR = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
)

env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)
# reading .env file
environ.Env.read_env()

# common settings
DEBUG = env("DEBUG", default=False)
SECRET_KEY = env("SECRET_KEY", default="YOUR_SECRET_KEY")
ALLOWED_HOSTS = ["*"]

ROOT_URLCONF = "demo.urls"
WSGI_APPLICATION = "demo.wsgi.application"
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# https://github.com/adamchainz/django-cors-headers
CORS_ALLOWED_ORIGINS = ["*"]
CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_CREDENTIALS = False
default_headers = (
    "accept",
    "accept-encoding",
    "authorization",
    "content-type",
    "dnt",
    "origin",
    "user-agent",
    "x-csrftoken",
    "x-requested-with",
)
CORS_ALLOW_METHODS = [
    "DELETE",
    "GET",
    "OPTIONS",
    "PATCH",
    "POST",
    "PUT",
]
