from .base import DEBUG

DJANGO_APPs = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

EXTERNAL_APPS = [
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",
    "django_extensions",
    "django_celery_beat",
    "django_celery_results",
    "django_filters",
]
INTERNAL_APPS = [
    "v1",
    "warehouse",
]

INSTALLED_APPS = DJANGO_APPs + EXTERNAL_APPS + INTERNAL_APPS

if DEBUG:
    INSTALLED_APPS += ["debug_toolbar"]
