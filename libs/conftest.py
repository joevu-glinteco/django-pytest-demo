import logging
from datetime import datetime

import pytest

module_fixture_call_count = 0
function_fixture_call_count = 0
session_fixture_call_count = 0
class_fixture_call_count = 0


@pytest.fixture(scope="function")
def function_fixture():
    global function_fixture_call_count

    function_fixture_call_count += 1
    logging.getLogger().info(
        f"function_fixture is called {function_fixture_call_count} times"
    )

    yield 2022


@pytest.fixture(scope="module")
def module_fixture():
    global module_fixture_call_count

    module_fixture_call_count += 1
    logging.getLogger().info(
        f"module_fixture is called {module_fixture_call_count} times"
    )

    yield datetime.now().date()


@pytest.fixture(scope="session")
def session_fixture():
    global session_fixture_call_count

    session_fixture_call_count += 1
    logging.getLogger().info(
        f"session_fixture_call_count is called {session_fixture_call_count} times"
    )

    yield "hello"


@pytest.fixture(scope="class")
def class_fixture(request):
    request.cls.year = 1988
    request.cls.current_age = 34

    global class_fixture_call_count

    class_fixture_call_count += 1
    logging.getLogger().info(
        f"class_fixture is called {class_fixture_call_count} times"
    )

    yield 2022
