from rest_framework import viewsets

from warehouse.models import Product

from ..filters import ProductFilter
from ..serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    filterset_class = ProductFilter
    queryset = Product.objects.all()
