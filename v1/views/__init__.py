from .category import CategoryViewSet
from .product import ProductViewSet

__all__ = [
    "ProductViewSet",
    "CategoryViewSet",
]
