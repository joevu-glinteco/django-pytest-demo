import pytest
from django.urls import reverse

from warehouse.factories import ProductFactory

pytestmark = pytest.mark.django_db


@pytest.mark.api
def test_products_list(client, json_headers, default_product):
    res = client.get(
        reverse("v1:products-list"),
        **json_headers,
    )

    assert res.status_code == 200

    ids = [product["id"] for product in res.json()["results"]]
    assert default_product.id in ids


@pytest.mark.api
def test_product_detail(client, json_headers, default_product):
    res = client.get(
        reverse("v1:products-detail", args=[default_product.id]),
        **json_headers,
    )

    assert res.status_code == 200

    data = res.json()
    assert data["name"] == default_product.name
    assert data["id"] == default_product.id
    assert data["code"] == default_product.code


@pytest.mark.api
def test_product_patch(client, json_headers, default_product):
    res = client.patch(
        reverse("v1:products-detail", args=[default_product.id]),
        {"name": "new name"},
        **json_headers,
    )

    assert res.status_code == 200

    data = res.json()
    default_product.refresh_from_db()
    assert data["name"] == default_product.name


@pytest.mark.api
def test_product_put(client, json_headers, default_product):
    res = client.put(
        reverse("v1:products-detail", args=[default_product.id]),
        {
            "name": "new name",
            "code": "unique1",
            "category": default_product.category.id,
            "quantity": 123,
        },
        **json_headers,
    )

    assert res.status_code == 200

    data = res.json()
    default_product.refresh_from_db()
    assert data["name"] == default_product.name
    assert data["quantity"] == default_product.quantity
    assert data["code"] == default_product.code
    assert data["category"] == default_product.category.id


@pytest.mark.api
def test_product_delte(client, json_headers):
    another_product = ProductFactory.create()
    res = client.delete(
        reverse("v1:products-detail", args=[another_product.id]),
        **json_headers,
    )

    assert res.status_code == 204
